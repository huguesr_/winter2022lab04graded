public class ApplianceStore{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Oven[] ovens = new Oven[3];
		
		for(int i=0;i<ovens.length;i++){
			System.out.println("Enter type, wattage and brand for oven "+(i+1)+":");
			ovens[i] = new Oven(reader.next(), reader.nextInt(), reader.next());
			
		}
		
		System.out.println(ovens[ovens.length-1].getType());
		System.out.println(ovens[ovens.length-1].getWattage());
		System.out.println(ovens[ovens.length-1].getBrand());
		
		System.out.println("Please modify type, wattage and brand for last oven");
		ovens[ovens.length-1].setType(reader.next());
		ovens[ovens.length-1].setWattage(reader.nextInt());
		ovens[ovens.length-1].setBrand(reader.next());
		
		
		
		ovens[0].electricCostForHour(ovens[0].getType(), ovens[0].getWattage());
		ovens[0].printBrandRep(ovens[0].getBrand());
		
		System.out.println("What food are you cooking? :");
		
		ovens[1].cook(reader.next());
		System.out.println(ovens[1].getCleanState());
		
	}
	
	
}