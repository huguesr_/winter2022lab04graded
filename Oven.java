public class Oven{
	private String type;
	private int wattage;
	private String brand;
	private String cleanState;
	
	public Oven(String type, int wattage, String brand){
		this.type = type;
		this.wattage = wattage;
		this.brand = brand;
		this.cleanState = "clean";
	}
	
	public void electricCostForHour(String type, int wattage){
		if (type.equals("gas")) {
			System.out.println("A gas oven doesn't use electricity...");
		}
		else {
			System.out.println(((wattage/1000)*7)+" cents per hour");
		}
		
	}
	public void printBrandRep(String brand){
		if (brand.equals("bosh")) {
			System.out.println("Bosh is Honestly not the best!");
		}
		else {
			System.out.println(brand+" is a pretty good brand!");
		}
	}
	public void cook(String foodCooked) {
		boolean foodValidation = checkFoodCooked(foodCooked);
		if (foodValidation){
			this.cleanState = "dirty";
		}
		else {
			this.cleanState = "clean";
			System.out.println("Invalid Food");
		}
	}
	private boolean checkFoodCooked(String foodCooked) {
		if (foodCooked.equals("lasagna") || foodCooked.equals("bread")) {
			return true;
		}
		else {
			return false;
		}
	}
	public String getType() {
		return this.type;
	}
	public int getWattage() {
		return this.wattage;
	}
	public String getBrand() {
		return this.brand;
	}
	public String getCleanState() {
		return this.cleanState;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public void setWattage(int wattage) {
		this.wattage = wattage;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}

}